import React from 'react';
import HeaderNav from './HeaderNav.jsx';
import MainView from './MainView/MainView.jsx';
import ChatInput from './ChatInput.jsx';
import Mustache from 'mustache'
import util from '../util.js'

var Web3 = require("web3");
var Eth = require('ethjs')

import commands from '../commands.js'
import config from '../../config.json'

class App extends React.Component {
    constructor(props){
        super(props)

        let nicknames = localStorage.getItem("nicknames")
        try {
            nicknames = JSON.parse(nicknames)
        }catch(e){ nicknames = null}

        this.state = {
            messages: [],
            userAddress: '',
            room: window.location.search.replace(/^\?/, ''),
            metadata: {},
            contracts: {},
            loggedIn: false,
            nicknames: nicknames || {}
        }

        // bind functions
        this.addChatEntry = this.addChatEntry.bind(this);
        this.addRoomInfo = this.addRoomInfo.bind(this);
        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this)
        this.parseMessage= this.parseMessage.bind(this);
        this.getNicknameOrAddress= this.getNicknameOrAddress.bind(this)
        this.setNickname = this.setNickname.bind(this)
        this.send = this.send.bind(this)

        //define component variables
        this.config = config;
        this.ws;

        // Checking if Web3 has been injected by the browser (Mist/MetaMask)
        if (typeof web3 !== 'undefined') {
            // Use Mist/MetaMask's provider
            this.provider = web3.currentProvider
            this.web3 = new Web3(web3.currentProvider);
            this.eth = new Eth(this.provider)
            this.roomContract = this.eth.contract(this.config.abi)

        } else {
            console.log("web3 injection failed")
            alert("web3 injection failed. You might want to install the metaMask extension.")
        }
    }

    // mount websocket && get default account
    async componentDidMount() {
        // fetch userAddress from metaMask
        if (this.eth) {
            var userAddress = (await this.eth.accounts())[0]
            this.setState({userAddress: userAddress})
        } else {
            console.log("you might want to unlock MetaMask")
        }

        //Websocketstuff
        if(document.domain == 'darq.chat'){
            this.ws = new WebSocket('wss://' + document.domain + ":" + location.port+ '/ws')
        }
        else {
            this.ws = new WebSocket('ws://' + document.domain + ":" + location.port+ '/ws')
        }


        this.ws.onopen = function() {
            console.log('wooo')
        }

        this.ws.onmessage = (message) => {
            var args = JSON.parse(message.data)
            var cmd = args.cmd
            if (cmd === 'message') {
                this.addChatEntry({text:args.text,
                                    type: args.type,
                                    from: args.from,
                                    time: args.time
                });
            }
            if (cmd === 'loggedIn') {
                this.setState({loggedIn: true})
            }
        }

        this.ws.onclose = function() {
            this.send({cmd:"logout"})
        }

        var pingWs = setInterval(() => {
            this.send({cmd:'ping'})
        }, 50000)
    }

    // declare methods
    send(data) {
        if (this.ws && this.ws.readyState === this.ws.OPEN) {
            this.ws.send(JSON.stringify(data))
        }
    }

    getUserAddress() {
        if (!this.state.userAddress) {
            alert("unlock MetaMask!")
            return false
        } else {return this.state.userAddress}
    }

    parseMessage(msg) {
        if(!msg) return
        //split and remove superfluous spaces
        let words = msg.split(' ').filter(word => word !== '')

        //check for nicknames
        words = words.map((word) => {
            if(word[0] === '@' ){
                if(this.getNicknameOrAddress(word.slice(1))){
                    return this.getNicknameOrAddress(word.slice(1))
                }
            }
            return word
        })

        if(words[0].charAt(0) !== '/'){
            this.send( {cmd: 'message', text: msg} )
        }
        else {
            // removing the leading slash
            var command = words[0].slice(1)
            switch(true){
                case command == 'online':
                    return commands.getOnline(this.send, this.state.room)

                case command == 'info':
                    return commands.roomInfo(this.addChatEntry, this.state.metadata, this.state.contracts)

                case command == 'help':
                    return commands.help(this.addChatEntry)

                case command == 'events':
                    return commands.events(this.addChatEntry, this.web3, this.state.contracts, words.slice(1))

                case command == 'loadContract':
                    return commands.loadContract(this.addChatEntry, this.addContractInfo.bind(this), words.slice(1))

                case command == 'whspr':
                    return commands.whisper(this.addChatEntry, this.send, this.state.loggedIn, words.slice(1))

                case command == 'sendEther':
                    return commands.sendEther(this.addChatEntry,
                                              this.send.bind(this),
                                              this.getUserAddress.bind(this),
                                              this.eth,
                                              words.slice(1)
                    )
                case command === 'nick':
                    return commands.nick(this.addChatEntry, this.setNickname, this.state.nicknames, words.slice(1))

                case command in this.state.contracts:
                    return commands.sendTransaction(this.addChatEntry,
                                                    this.getUserAddress.bind(this),
                                                    this.eth,
                                                    this.state.contracts[command],
                                                    words
                    )

                default:
                    return this.addChatEntry({text:"/" + command +" is not a valid command. Try /help", type: "notification"})
            }
        }
    }

    addChatEntry(msg) {
        this.setState({
            messages: this.state.messages.concat({
                from: msg.from,
                type: msg.type || "message", //older messages might not have a type
                time: msg.time,
                text: msg.text
            })
        })
    }

    addRoomInfo(metadata) {
        var newMeta = {}
        var contracts = {}

        if("abi" in metadata){
            let substitutions = {}
            let parameters = this.state.room.split('/').slice(1)
            let getPermissions = metadata.abi.find((func) =>{return func.name === "getPermissions"}).inputs.slice(1)

            for(var arg in getPermissions){
                substitutions[getPermissions[arg].name] = parameters[arg]
            }

            metadata = JSON.parse(Mustache.render(JSON.stringify(metadata), substitutions))
        }


        if ("name" in metadata) {
            newMeta["name"] = metadata.name;
        }
        if ("description" in metadata) {
            newMeta["description"] = metadata.description;
        }
        if("Contracts" in metadata){
            for( var i in metadata.Contracts){
                var contractData = metadata.Contracts[i]
                this.addContractInfo(contractData)
            }
        }
        this.setState({ metadata: newMeta})
    }

    addContractInfo(contractData) {
        var contracts = this.state.contracts
        if("name" in contractData &&
           "abi" in contractData &&
           "address" in contractData) {
            contracts[contractData.name] = {abi: contractData.abi, address: contractData.address}
            this.setState({contracts: contracts })
            return true;
        }
    }


    logout() {
        this.send({cmd: 'logout'})
        this.setState({loggedIn: false})
    }

    async login () {
        var accounts = await this.eth.accounts()
        if (accounts.length === 0) {
            alert("unlock MetaMask!")
        } else {
            var block = await this.eth.getBlockByNumber("latest", true)
            var rawMsg = {blockHash: block.hash, site: 'chat'}
            var msg = '0x' + (new Buffer(JSON.stringify(rawMsg), 'utf8')).toString('hex')
            var defaultAddress = accounts[0]
            var params = [msg, defaultAddress]
            var method = 'personal_sign'

            this.provider.sendAsync({
                method,
                params,
                defaultAddress,
            }, (err, res) => {
                if(!err && !res.error) {
                    this.send({cmd: 'login', data: msg, sig: res.result, from: defaultAddress, room: this.state.room})
                } else {console.log(err,res)}
            })
        }
    }

    setNickname(address, nick){
        if (!util.isAddress(address)){
            return false;
        }
        var nicks = this.state.nicknames

        if(!nick){
            delete nicks[this.getNicknameOrAddress(address)]
            delete nicks[address]
        }
        else {
            nicks[address] = nick
            nicks[nick] = address
        }
        this.setState({nicknames: nicks})
        localStorage.setItem("nicknames", JSON.stringify(this.state.nicknames))
        return true;
    }

    getNicknameOrAddress(addressOrName){
        if(this.state.nicknames.hasOwnProperty(addressOrName) && this.state.nicknames[addressOrName]) {
            return this.state.nicknames[addressOrName]
        }
        return false;
    }

    // life cycle methods
    render() {
        return (
            <div style={{
                padding: "30px",
                paddingTop: '10px',
                width: '100%',
                height: "100%",
                backgroundColor: "#9b9b9b"
            }}>
                <HeaderNav login={this.login} logout={this.logout} loggedIn={this.state.loggedIn}/>
                <MainView
                    room={this.state.room}
                    roomContract={this.roomContract}
                    metadata={this.state.metadata}
                    messages={this.state.messages}
                    getNicknameOrAddress = {this.getNicknameOrAddress}
                    addRoomInfo={this.addRoomInfo}
                    config={this.config}
                />
                <br/>
                <ChatInput
                    parseMessage={this.parseMessage}
                />
            </div>
        );
    }
}

export default App;
